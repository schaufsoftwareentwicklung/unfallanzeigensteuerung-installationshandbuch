\babel@toc {german}{}
\contentsline {chapter}{\numberline {1}Einleitung}{3}{chapter.1}% 
\contentsline {section}{\numberline {1.1}Desktopinstallation}{3}{section.1.1}% 
\contentsline {chapter}{\numberline {2}Serverinstallation}{5}{chapter.2}% 
\contentsline {section}{\numberline {2.1}Anforderungen}{5}{section.2.1}% 
\contentsline {section}{\numberline {2.2}Internetinformationsdienst installation}{5}{section.2.2}% 
\contentsline {section}{\numberline {2.3}MongoDB installieren}{12}{section.2.3}% 
\contentsline {section}{\numberline {2.4}Energieoptionen}{20}{section.2.4}% 
\contentsline {section}{\numberline {2.5}Backend installieren}{22}{section.2.5}% 
\contentsline {section}{\numberline {2.6}Frontend installieren}{26}{section.2.6}% 
